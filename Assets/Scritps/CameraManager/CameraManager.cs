﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class CameraManager : MonoBehaviour
{

	public GameObject[] Player_Obj = new GameObject[4];



	public void SetPlayerSolo(int _PlayerCount)
	{
		List<int> TargetList = new List<int>();
		TargetList.Add(_PlayerCount);
		SetCameraTarget(TargetList);
	}



	public void EndPlayerSolo()
	{
		List<int> TargetList = new List<int>();
		TargetList.Add(0);
		TargetList.Add(1);
		TargetList.Add(2);
		TargetList.Add(3);
		SetCameraTarget(TargetList);
	}



	void Start()
    {
		mCamera2D = this.GetComponent<ProCamera2D>();
	}


    void Update()
    {

	}


	private void SetCameraTarget(List<int> _PlayerIndexList)
	{
		mCamera2D.RemoveAllCameraTargets();
		mCurrentTarget.Clear();

		for (int i = 0; i < _PlayerIndexList.Count; i++)
		{
			if (_PlayerIndexList[i] > Player_Obj.Length)
			{
				continue;
			}
			mCamera2D.AddCameraTarget(Player_Obj[_PlayerIndexList[i]].transform);
		}
	}

	private List<int> mCurrentTarget = new List<int>();

	private ProCamera2D mCamera2D;
	private ProCamera2DZoomToFitTargets mZoomToFit;
}
