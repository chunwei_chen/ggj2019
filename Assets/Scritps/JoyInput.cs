﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyInput : MonoBehaviour
{
	protected float m_JoyX = 0.0f;
	protected float m_JoyY = 0.0f;

	public string mJoyName_H = "";
	public string mJoyName_V = "";
	public string mJoyName_Attack = "";
	public string mJoyName_Cool = "";


	public float GetJoyX()
	{
		m_JoyX = Input.GetAxis(mJoyName_H);
		return m_JoyX;
	}

	public float GetJoyY()
	{
		m_JoyY = Input.GetAxis(mJoyName_V);
		return m_JoyY;
	}

	public bool GetJoyAttack()
	{
		if (Input.GetButtonDown(mJoyName_Attack))
		{
			return true;
		}
		return false;
	}

	public bool GetJoyCool()
	{
		if (Input.GetButtonDown(mJoyName_Cool))
		{
			return true;
		}
		return false;
	}
	void Start()
	{

	}

	void Update()
	{
	}
}
