﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager s_Instance = null;
	void Awake()
	{
		if (s_Instance == null) { s_Instance = this; }
	}

	float mDadmatMax = 0.7f;
	float mCharmatMax = 0.65f;
	float mSisBromatMax = 0.5f;

	public GameObject ReadyFX;

	public Text mTextFogProgress;
	public Text mTextSpwanTimer;
	public Text mTextSpwanCount;
	public GameObject PauseUI;
	public GameObject GameOverUI;

	public Enemy mEnemyPrefab1;
	public Enemy mEnemyPrefab2;
	public Enemy mEnemyPrefab3;
	public List<Enemy> mEnemyList;
	public List<Character> mPlayerList;

	public List<Character> PlayerDieList;

	public float mFogProgress = 0.0f;
	public float mSpawnDuration = 0.0f;
	public int mSpawnCount = 1;

	public int mKillCount = 0;


	public float mTimer = 0.0f;
	public float mFogRate = 3.0f;
	public float mMaxSpawnSpeed = 0.2f;
	public float mMaxFogRate = 4.0f;
	public float mMinFogRate = 0.3f;
	public float mFogDeBuff = 0.001f;
	public float mKillRate = 0.1f;

	bool mStartSpawn = false;
	float mTOGETHERtime = 0.0f;
	bool mTOGETHER = false;
	float mSuperTime = 2.0f;

	Vector2 mSpawnLimitM = new Vector2(6.5f, 3.1f);
	Vector2 mSpawnLimitN = new Vector2(-6.5f, -4.2f);

	public bool Pause = false;

	public SpriteRenderer BGMat;
	public SpriteRenderer WallMat;


	bool isWIN = false;

	void Start()
	{
		
	}

	public void GoSpawn()
	{
		mStartSpawn = true;
	}

	public List<Enemy> GetEnemy()
	{
		return mEnemyList;
	}

	public void PlayerDieOrLife(Character aCharacter,bool aIsDie)
	{
		if (aIsDie)
		{
			PlayerDieList.Add(aCharacter);
			if (PlayerDieList.Count >= 4)
			{
				GameOver();
			}
		}
		else
		{
			PlayerDieList.Remove(aCharacter);
		}
	}

	void GameOver()
	{
		Pause = true;
		GameOverUI.SetActive(true);
		if (isWIN)
		{
			FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
			masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
			SceneManager.LoadScene(4);
		}
		else
		{
			FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
			masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
			SceneManager.LoadScene(3);
		}
	}

	void ReLoadGameScene()
	{
		FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
		masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
		SceneManager.LoadScene(0);
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.C))
		{
			FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
			masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
			SceneManager.LoadScene(4);
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (!Pause) { Pause = true; PauseUI.SetActive(true); }
			else
			if (Pause) { Pause = false; PauseUI.SetActive(false); }
		}
		if (Input.GetKeyDown(KeyCode.R))
		{
			ReLoadGameScene();
			//Application.LoadLevel(0);
		}
		if (Pause) return;
		if (Input.GetKeyDown(KeyCode.S))
		{
			GoSpawn();
		}
		if (!mStartSpawn) return;
		mTimer += Time.deltaTime;
		float mRate = 1f;


		if (mFogRate - mFogDeBuff > mMinFogRate)
		{
			if (mFogProgress < 20)
			{ mRate = 1f; }
			else if (mFogProgress < 50)
			{ mRate = 1.5f; }
			else if (mFogProgress < 70)
			{ mRate = 2f; }
			else
			{ mRate = 3f; }

			mFogRate -= (mFogDeBuff * mRate);
		}

		mSpawnCount = (int)Mathf.Floor(mFogProgress / 10) + 1;


		mFogProgress += (Time.deltaTime / mFogRate);
		if (mFogProgress >= 100)
		{
			mFogProgress = 100;
			GameOver();
		}


		BGMat.material.SetFloat("_Mask",(mFogProgress) / (100));
		if(mFogProgress>20)
		{ WallMat.material.SetFloat("_Mask", (mFogProgress) / (100)); }
		mPlayerList[0].GetComponent<SpriteRenderer>().material.SetFloat("_Mask", (mFogProgress /100* mDadmatMax) );
		mPlayerList[1].GetComponent<SpriteRenderer>().material.SetFloat("_Mask", (mFogProgress /100* mSisBromatMax) );
		mPlayerList[2].GetComponent<SpriteRenderer>().material.SetFloat("_Mask", (mFogProgress /100* mCharmatMax) );
		mPlayerList[3].GetComponent<SpriteRenderer>().material.SetFloat("_Mask", (mFogProgress /100* mCharmatMax) );


		if (mTimer > mSpawnDuration)
		{
			SpawnEnemy(); ;
			mTimer = 0;
		}

		if (CheckTOGETHER())
		{
			mTOGETHERtime += Time.deltaTime;
			if (mTOGETHERtime > mSuperTime)
			{
				isWIN = true;
				mTOGETHER = true;
				ReadyFX.SetActive(true);
				ReadyFX.transform.position = mPlayerList[0].transform.position;
				print("WINWIN");
			}
		}
		else
		{
			mTOGETHERtime = 0.0f;
		}

		if (Input.GetKeyDown(KeyCode.Z))
		{
			UpdateKillCount();
		}
		if (Input.GetKeyDown(KeyCode.X))
		{
			SpawnEnemy();
		}
		if (Input.GetKeyDown(KeyCode.V))
		{
			mFogProgress += 10;
		}

		//CheckSpriteOrder();
	}

	public void UpdateKillCount()
	{
		mKillCount++;

		if (mFogRate + mKillRate < mMaxFogRate)
		{
			mFogRate += mKillRate;
		}
		
	}

	bool CheckTOGETHER()
	{
		bool aIsReady = false;
		float Distance1to2 = Vector3.Distance(mPlayerList[0].transform.position, mPlayerList[1].transform.position);
		float Distance1to3 = Vector3.Distance(mPlayerList[0].transform.position, mPlayerList[2].transform.position);
		float Distance1to4 = Vector3.Distance(mPlayerList[0].transform.position, mPlayerList[3].transform.position);

		float Distance2to1 = Vector3.Distance(mPlayerList[1].transform.position, mPlayerList[0].transform.position);
		float Distance2to3 = Vector3.Distance(mPlayerList[1].transform.position, mPlayerList[2].transform.position);
		float Distance2to4 = Vector3.Distance(mPlayerList[1].transform.position, mPlayerList[3].transform.position);

		float Distance3to1 = Vector3.Distance(mPlayerList[2].transform.position, mPlayerList[0].transform.position);
		float Distance3to2 = Vector3.Distance(mPlayerList[2].transform.position, mPlayerList[1].transform.position);
		float Distance3to4 = Vector3.Distance(mPlayerList[2].transform.position, mPlayerList[3].transform.position);

		float Distance4to1 = Vector3.Distance(mPlayerList[3].transform.position, mPlayerList[0].transform.position);
		float Distance4to2 = Vector3.Distance(mPlayerList[3].transform.position, mPlayerList[1].transform.position);
		float Distance4to3 = Vector3.Distance(mPlayerList[3].transform.position, mPlayerList[2].transform.position);

		if (Distance1to2 < 0.5f &&
			Distance1to3 < 0.5f &&
			Distance1to4 < 0.5f &&
			Distance2to1 < 0.5f &&
			Distance2to3 < 0.5f &&
			Distance2to4 < 0.5f &&
			Distance3to1 < 0.5f &&
			Distance3to2 < 0.5f &&
			Distance3to4 < 0.5f &&
			Distance4to1 < 0.5f &&
			Distance4to2 < 0.5f &&
			Distance4to3 < 0.5f)
		{
			aIsReady = true;
			print("Oh YA!");
		}
		else
		{
			isWIN = false;
			ReadyFX.SetActive(false);
		}

		return aIsReady;
	}



	void OnGUI()
	{
		if (Pause) return;
		mTextFogProgress.text = mFogProgress.ToString();
		mTextSpwanTimer.text  = mFogRate.ToString();
		mTextSpwanCount.text  = mSpawnCount.ToString();

	}

	void SpawnEnemy()
	{
		Enemy aSpawn;
		if (mFogProgress < 20)
		{
			aSpawn = mEnemyPrefab1;
		}
		else
		if (mFogProgress < 60)
		{
			aSpawn = mEnemyPrefab2;
		}
		else
		{
			aSpawn = mEnemyPrefab3;
		}
		for (int aIndex = 0; aIndex < mSpawnCount; aIndex++)
		{
			Vector3 aSpawnPos = new Vector3(Random.Range(mSpawnLimitN.x, mSpawnLimitM.x), Random.Range(mSpawnLimitN.y, mSpawnLimitM.y), 0);
			Enemy aEnemy = Instantiate(aSpawn, aSpawnPos, Quaternion.identity);
			mEnemyList.Add(aEnemy);
		}
	}



	/*void CheckSpriteOrder()
	{
		for (int i = 1; i <= mPlayerList.Count - 1; i++)//執行的回數
		{
			for (int j = 1; j <= mPlayerList.Count - i; j++)//執行的次數
			{
				if (list[j] < list[j - 1])
				{
					//二數交換
					int temp = list[j];
					list[j] = list[j - 1];
					list[j - 1] = temp;
				}
			}
		}
	}*/


}
