﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menu : MonoBehaviour
{
	// Start is called before the first frame update

	public string path;

	void Start()
	{
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.anyKey)
		{
			FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
			masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
			SceneManager.LoadScene(1);
		}
    }

}
