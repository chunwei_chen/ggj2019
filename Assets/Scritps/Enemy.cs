﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	EnemyAnimation enemyAnimation;
	Character mTarget;

	public FXScript fx;
	public FXScript Atkfx;

	float mTime = 0;
	float mDistance = 99;


	public float mSpeed = 1.0f;
	public float mDelayTime = 2.0f;
	public float mAttackRange = 0.8f;
	public int mblood = 1;

	int nowBlood = 1;
	bool mCantmove = false;

	public bool isDead = false;

	void Start()
	{
		nowBlood = mblood;
		enemyAnimation = gameObject.GetComponent<EnemyAnimation>();
	}

	void Update()
	{
		if (isDead)
		{
			Destroy(this.gameObject);
		}
		if (mCantmove) { StopAllCoroutines(); return; }
		if (GameManager.s_Instance.Pause)
		{
			StopAllCoroutines();
			return;
		}
		mTime += Time.deltaTime;
		if (mTarget == null)
		{
			mTarget = GetTarget();
		}
		else
		{
			mDistance = Vector3.Distance(this.transform.position, mTarget.transform.position);
			if (mDistance > mAttackRange)
			{
				this.transform.position = Vector3.MoveTowards(transform.position, mTarget.transform.position, mSpeed * Time.deltaTime);
				enemyAnimation.SetFacing(mTarget.transform.position.x - transform.position.x);
			}
			else
			{
				DoAttack();
			}
		}
		
	}
	void DoAttack()
	{
		mTarget = null;
		mDistance = 99;
		StartCoroutine(AttackCalcurate());
	}


	public bool UnderFire(int aDamage)
	{
		bool aIsDead = false;
		//enemyAnimation.HurtAnim();
		if (nowBlood - aDamage <= 0)
		{
			aIsDead = true;
			nowBlood -= aDamage;
			GameManager.s_Instance.mEnemyList.Remove(this);
			enemyAnimation.HurtAnim();
			mCantmove = true;
			//Destroy(this.gameObject);
		}
		else
		{
			nowBlood -= aDamage;
		}

		Instantiate(fx, this.transform.position, Quaternion.identity);

		return aIsDead;
	}


	IEnumerator AttackCalcurate()
	{
		yield return new WaitForSeconds(1.0f);

		float aNearestDistance = 99;
		Character aTarget = null;
		List<Character> aCharacters = GameManager.s_Instance.mPlayerList;
		for (int aIndex = 0; aIndex < 4; aIndex++)
		{
			Character aCharacter = aCharacters[aIndex];
			if (aCharacter.mIsDead) continue;
			float aDistance = Vector3.Distance(this.transform.position, aCharacter.transform.position);
			if (aDistance < mAttackRange)
			{
				enemyAnimation.AttackAnim();
				aCharacter.UnderAttack();
				Instantiate(Atkfx, aCharacter.transform.position, Quaternion.identity);
			}
		}
	}

	Character GetTarget()
	{
		float aMostNearDistance = 99f;
		List<Character> aCharacters = GameManager.s_Instance.mPlayerList;
		Character aTarget = aCharacters[0];
		for (int aIndex = 0; aIndex < 4; aIndex++)
		{
			Character aCharacter = aCharacters[aIndex];
			if (aCharacter.mIsDead) continue;
			float aDistance = Vector3.Distance(this.transform.position, aCharacter.transform.position);
			if (aDistance < aMostNearDistance)
			{
				aMostNearDistance = aDistance;
				aTarget = aCharacter;
			}
		}
		return aTarget;
	}

}
