﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SisBullet : MonoBehaviour
{

	public SpriteRenderer mSpriteRenderer;
	float mMoveRange = 2.0f;
	public float mAttackDire = 1;
	public Character mBro;
	public float mSpeed = 1f;
	bool mDoRollback = false;
    // Start is called before the first frame update
    void Start()
    {
		
		StartCoroutine(DoKick());
	}

	IEnumerator DoKick()
	{
		float temp = 1.0f;

		float aFloory = this.transform.position.y- 0.3f;
		
		while (this.transform.position.y > aFloory)
		{
			yield return new WaitForSeconds(0.01f);


			this.transform.position  += new Vector3(0.3f * mAttackDire, -0.05f, 0);


			List<Enemy> aEnemyList = new List<Enemy>();
			aEnemyList = GameManager.s_Instance.GetEnemy();
			if (aEnemyList.Count == 0) continue;
			Enemy aAttackTarget = aEnemyList[0];
			for (int aIndxe = 0; aIndxe < aEnemyList.Count; aIndxe++)
			{
				if (aEnemyList[aIndxe] == null) continue;
				float aDistance = 0.0f;
				aDistance = Vector3.Distance(this.transform.position, aEnemyList[aIndxe].transform.position);
				if (aDistance < 0.5f)
				{
					if(aEnemyList[aIndxe].UnderFire(3))
					{
						mBro.KillEnemy();
					}
				}
			}
		}
		yield return new WaitForSeconds(0.2f);
		mDoRollback = true;
	}

	IEnumerator DoBack()
	{

		mBro.characterAnimation.mIsBroAlong = false;
		yield return new WaitForSeconds(0.4f);
		Destroy(this.gameObject);

		mBro.mIsSisShooting = false;
	}

		// Update is called once per frame
	void Update()
    {
		if (mDoRollback)
		{
			if (Vector3.Distance(this.transform.position, mBro.transform.position) > 0.5f)
			{
				Vector3 aTarger = new Vector3(mBro.transform.position.x, mBro.transform.position.y-0.35f, mBro.transform.position.z);
				this.transform.position = Vector3.MoveTowards(transform.position, aTarger, mSpeed * Time.deltaTime);
				List<Enemy> aEnemyList = new List<Enemy>();
				aEnemyList = GameManager.s_Instance.GetEnemy();
				if (aEnemyList.Count == 0) return ;
				Enemy aAttackTarget = aEnemyList[0];
				for (int aIndxe = 0; aIndxe < aEnemyList.Count; aIndxe++)
				{
					if (aEnemyList[aIndxe] == null) continue;
					float aDistance = 0.0f;
					aDistance = Vector3.Distance(this.transform.position, aEnemyList[aIndxe].transform.position);
					if (aDistance < 0.5f)
					{
						if (aEnemyList[aIndxe].UnderFire(3))
						{
							mBro.KillEnemy();
						}
					}
				}
			}
			else
			{
				StartCoroutine(DoBack());
			}
		}

       // this.transform.position += 
    }
}
