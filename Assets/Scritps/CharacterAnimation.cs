﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
	public Animator mAnim;
	public SpriteRenderer mSpriteRenderer;
	public bool mIsFacingRight = false;
	public bool mIsIdle = false;
	public bool mIsWalk = false;
	public bool mIsDead = false;
	public int mIsCombo = 0;
	public string[] mAttackAnimName;
	public string mCoolAnimName;


	public bool mIsBroAlong = false;

	void Start()
	{
		mAnim = gameObject.GetComponent<Animator>();
		mSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

	void Update()
	{
		if (mAnim)
		{
			mAnim.SetBool("IsIdle", mIsIdle);
			mAnim.SetBool("IsBroAlong", mIsBroAlong);
			mAnim.SetBool("IsWalk", mIsWalk);
			mAnim.SetBool("IsDead", mIsDead);
			mAnim.SetInteger("IsCombo", mIsCombo);
		}
	}
	public bool SetFacing(float aInput)
	{
		if (aInput > 0)
		{
			if (!mIsFacingRight)
			{
				mSpriteRenderer.flipX = true;
				mIsFacingRight = true;
			}
			return true;
		}
		else
		{
			if (mIsFacingRight)
			{
				mSpriteRenderer.flipX = false;
				mIsFacingRight = false;
			}
			return false;
		}
	}

	public void AttackAnim()
	{
		if (mAnim)
		{
			mAnim.SetTrigger("IsAttack");
		}
	}

	public void CoolAnim()
	{
		if (mAnim)
		{
			mAnim.SetTrigger("IsCool");
		}
	}

	public bool IsPlayingAttackAnim()
	{
		if (mAnim)
		{
			if (mAnim.GetCurrentAnimatorStateInfo(0).IsName(mAttackAnimName[mIsCombo]))
			{
				return true;
			}
		}
		return false;
	}
	public bool IsPlayingCoolAnim()
	{
		if (mAnim)
		{
			if (mAnim.GetCurrentAnimatorStateInfo(0).IsName(mCoolAnimName))
			{
				return true;
			}
		}
		return false;
	}
}
