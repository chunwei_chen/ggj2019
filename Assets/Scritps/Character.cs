﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
	public JoyInput joyInput;
	public CharacterAnimation characterAnimation;
	public float mSensitivity = 0.0f;
	public Text mTextIndex;
	public Text mTextScore;
	public int gPlayerIndex = 0;
	public int mMaxCombo = 0;

	public FXScript mIWLLBEBACK;
	public SisBullet mSisbullet;
	public bool mIsSisShooting = false;


	public Sprite BroSis;
	public Sprite Bro;
	Vector2 mDirection = new Vector2(0, 0);
	Vector2 mMoveLimitM = new Vector2(6.5f, 3.1f);
	Vector2 mMoveLimitN = new Vector2(-6.5f, -4.2f);
	public bool mIsDead = false;

	float mNowDirection = 1;

	public float mAttaclDelay=0f;
	float mNowDelay = 0f;

	public float mSpeedPlus = 1.0f;
	float AttackRange = 0.0f;
	int mScore = 0;
	public float mAttackrange = 0.8f;
	SpriteRenderer mRender;


	bool mGranpaAtkAni = false;










	private void Start()
	{
		mTextIndex.text = gPlayerIndex.ToString();
		joyInput = gameObject.GetComponent<JoyInput>();
		characterAnimation = gameObject.GetComponent<CharacterAnimation>();
		mRender = this.GetComponent<SpriteRenderer>();
	}

	public void ReLife()
	{
		Instantiate(mIWLLBEBACK, this.transform.position, Quaternion.identity);
		mIsDead = false;
		mRender.enabled = true;
		characterAnimation.mIsDead = false;
		GameManager.s_Instance.PlayerDieOrLife(this, false);
	}

	public void UnderAttack()
	{
		mIsDead = true;
		//mRender.enabled = false;
		characterAnimation.mIsDead = true;
		GameManager.s_Instance.PlayerDieOrLife(this, true);
	}

	void Update()
	{
		if (GameManager.s_Instance.Pause)
		{
			characterAnimation.mIsIdle = true;
			characterAnimation.mIsWalk = false;
			return;
		}
		if (mIsDead) return;

		if (characterAnimation)
		{
			if (joyInput.GetJoyCool())
			{
				characterAnimation.CoolAnim();
			}
			else if (joyInput.GetJoyAttack())
			{
				characterAnimation.AttackAnim();
				if (gPlayerIndex == 2)
				{
					if (mIsSisShooting) return;
				}
				if (gPlayerIndex == 3) { mGranpaAtkAni = true; StartCoroutine(DoRash()); }
				DoAttack();
				if (characterAnimation.IsPlayingAttackAnim())
				{
					if (characterAnimation.mIsCombo < mMaxCombo)
					{
						characterAnimation.mIsCombo++;
					}
					else
					{
						characterAnimation.mIsCombo = 0;
					}
				}
			}
			else if (!characterAnimation.IsPlayingAttackAnim() && !characterAnimation.IsPlayingCoolAnim())
			{
				DoMoving();
			}
		}
		if (gPlayerIndex == 2)
		{
			if (mIsSisShooting)
			{
				this.GetComponent<SpriteRenderer>().sprite = Bro;
			}
			else
			{
				this.GetComponent<SpriteRenderer>().sprite = BroSis;
			}
		}
	
		
	}

	IEnumerator DoRash()
	{
		yield return new WaitForSeconds(0.1f);
		float atemp = 1f;
		while (atemp > 0f)
		{
			yield return new WaitForSeconds(0.01f);
			atemp -= 0.1f;
			this.transform.position += new Vector3(0.1f * mNowDirection, 0, 0);

			List<Enemy> aEnemyList = new List<Enemy>();
			aEnemyList = GameManager.s_Instance.GetEnemy();
			if (aEnemyList.Count == 0) continue;
			Enemy aAttackTarget = aEnemyList[0];
			for (int aIndxe = 0; aIndxe < aEnemyList.Count; aIndxe++)
			{
				if (aEnemyList[aIndxe] == null) continue;
				float aDistance = 0.0f;
				aDistance = Vector3.Distance(this.transform.position, aEnemyList[aIndxe].transform.position);
				if (aDistance < 1.5f)
				{
					if (aEnemyList[aIndxe].UnderFire(2))
					{
						KillEnemy();
					}
				}
			}
		}
	}

	void DoAttack()
	{
		if (gPlayerIndex == 1)
		{
			float aNeariestDistance = 99;
			List<Enemy> aEnemyList = new List<Enemy>();
			aEnemyList = GameManager.s_Instance.GetEnemy();
			if (aEnemyList.Count == 0) return;
			Enemy aAttackTarget = aEnemyList[0];
			for (int aIndxe = 0; aIndxe < aEnemyList.Count; aIndxe++)
			{
				if (aEnemyList[aIndxe] == null) continue;
				float aDistance = 0.0f;
				aDistance = Vector3.Distance(this.transform.position, aEnemyList[aIndxe].transform.position);
				if (aDistance < aNeariestDistance)
				{
					aNeariestDistance = aDistance;
					aAttackTarget = aEnemyList[aIndxe];
				}
			}
			if (aNeariestDistance < mAttackrange)
			{
				if (aAttackTarget != null)
				{
					if (aAttackTarget.UnderFire(3))
					{
						KillEnemy();
					}
					GameManager.s_Instance.UpdateKillCount();
				}
			}
		}
		if (gPlayerIndex == 2)
		{
			Vector3 aBulletPos = new Vector3(this.transform.position.x + 1f * mNowDirection, this.transform.position.y, 1);

			SisBullet aBullet = Instantiate(mSisbullet, this.transform.position, Quaternion.identity);
			aBullet.mAttackDire = mNowDirection;
			aBullet.mBro = this;
			if (mNowDirection > 0)
			{
				aBullet.mSpriteRenderer.flipX = true;
			}
			characterAnimation.mIsBroAlong = true;
			mIsSisShooting = true;
		}
		if (gPlayerIndex == 4 )
		{
			List<Enemy> aEnemyList = new List<Enemy>();
			aEnemyList = GameManager.s_Instance.GetEnemy();
			if (aEnemyList.Count == 0) return;
			Enemy aAttackTarget = aEnemyList[0];
			for (int aIndxe = 0; aIndxe < aEnemyList.Count; aIndxe++)
			{
				if (aEnemyList[aIndxe] == null) continue;

				float aDistance = Vector3.Distance(this.transform.position, aEnemyList[aIndxe].transform.position);

				if (aDistance < 1.0f)
				{
					if (mNowDirection > 0)
					{
						if (aEnemyList[aIndxe].transform.position.x < transform.position.x)
						{
							if(aEnemyList[aIndxe].UnderFire(1))
							{
								KillEnemy();
							}
						}
					}
					else
					{
						if (aEnemyList[aIndxe].transform.position.x > transform.position.x)
						{
							if (aEnemyList[aIndxe].UnderFire(1))
							{
								KillEnemy();
							}
						}
					}
					if (mMaxCombo == characterAnimation.mIsCombo)
					{
						aEnemyList[aIndxe].transform.position += new Vector3(mNowDirection * 2f, 0, 0);
					}
				}
			}
		}

	}
	public void KillEnemy()
	{
		mScore++;
		GameManager.s_Instance.UpdateKillCount();
	}


	void CheckOtherPlayer()
	{
		for (int aIndex = 0; aIndex < GameManager.s_Instance.mPlayerList.Count; aIndex++)
		{
			Character aChracter = GameManager.s_Instance.mPlayerList[aIndex];
			if (aChracter == this)
			{ continue; }
			else
			{
				if (aChracter.mIsDead)
				{
					float aDistance = Vector3.Distance(this.transform.position, aChracter.transform.position);
					if (aDistance < 0.5f)
					{
						aChracter.ReLife();
					}
				}
			}
		}
	}

	void DoMoving()
	{
			characterAnimation.mIsCombo = 0;
		if (joyInput.GetJoyY() > mSensitivity)
		{
			mDirection = new Vector2(mDirection.x, -joyInput.GetJoyY());
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = false;
				characterAnimation.mIsWalk = true;
			}
		}
		else if (joyInput.GetJoyY() < -mSensitivity)
		{
			mDirection = new Vector2(mDirection.x, -joyInput.GetJoyY());
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = false;
				characterAnimation.mIsWalk = true;
			}
		}
		else
		{
			mDirection = new Vector2(mDirection.x, 0);
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = true;
				characterAnimation.mIsWalk = false;
			}
		}

		if (joyInput.GetJoyX() > mSensitivity)
		{
			mDirection = new Vector2(joyInput.GetJoyX(), mDirection.y);
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = false;
				characterAnimation.mIsWalk = true;
				characterAnimation.SetFacing(joyInput.GetJoyX());

				mNowDirection = 1;
			}
		}
		else if (joyInput.GetJoyX() < -mSensitivity)
		{
			mDirection = new Vector2(joyInput.GetJoyX(), mDirection.y);
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = false;
				characterAnimation.mIsWalk = true;
				characterAnimation.SetFacing(joyInput.GetJoyX());
				mNowDirection = -1;
			}
		}
		else
		{
			mDirection = new Vector2(0, mDirection.y);
			if (characterAnimation)
			{
				characterAnimation.mIsIdle = true;
				characterAnimation.mIsWalk = false;
			}
		}

		if (mDirection != new Vector2(0, 0))
		{
			characterAnimation.mIsIdle = false;
			characterAnimation.mIsWalk = true;
		}
		else
		{
			characterAnimation.mIsIdle = true;
			characterAnimation.mIsWalk = false;
		}


		Vector3 aMovingVector = new Vector3(mDirection.x, mDirection.y, 0);
		this.transform.position += (aMovingVector * mSpeedPlus) / 50;
		CheckOtherPlayer();

		if (this.transform.position.x > mMoveLimitM.x)
		{
			this.transform.position = new Vector3(mMoveLimitM.x, this.transform.position.y, 1);
		}
		if (this.transform.position.x < mMoveLimitN.x)
		{
			this.transform.position = new Vector3(mMoveLimitN.x, this.transform.position.y, 1);
		}

		if (this.transform.position.y > mMoveLimitM.y)
		{
			this.transform.position = new Vector3(this.transform.position.x, mMoveLimitM.y, 1);
		}
		if (this.transform.position.y < mMoveLimitN.y)
		{
			this.transform.position = new Vector3(this.transform.position.x, mMoveLimitN.y, 1);
		}
	}

	void OnGUI()
	{
		string temp = "";
		if (mScore < 10)
		{
			temp = "0" + mScore.ToString();
		}
		else
		{
			temp = mScore.ToString();
		}
		mTextScore.text = temp;
	}
}
