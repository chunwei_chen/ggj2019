﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
	public string mSoundPlayOnStart = "";
	
	private FMODUnity.StudioEventEmitter eventEmitterRef;
	string p = "";
	void Start()
	{
		eventEmitterRef = GetComponent<FMODUnity.StudioEventEmitter>();
		if (mSoundPlayOnStart != "")
		{
			PlayFMODSound(mSoundPlayOnStart);
		}
	}
	
    void Update()
    {
	
	}
	public void PlayFMODSound(string path)
	{
		p = path;
		//FMODUnity.RuntimeManager.PlayOneShotAttached(path, this.gameObject);
		FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
	}
}
