﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StoryLineType
{
	StartLine,
	LoopStartLine,
	BackToStartLine,
	GameStartLine,
	FirstPrompt,
	MidPrompt,
	LastPrompt,
	GoodPointPrompt,
	GoodEnd,
	BadEnd,
}

public enum Language
{
	EN,
	CH,
	JP,
}

public class StoryLineManager : MonoBehaviour
{

	public StoryLineObject[] StoryLineObjectData;
	public StoryLineObject   CurLine;
	public Text              TextUI;

	public Language CurLanguage = Language.CH;

	public StoryLineObject[] LoopStartList;
	public StoryLineObject[] FirstPromptList;
	public StoryLineObject[] MidPromptList;
	public StoryLineObject[] LastPromptList;
	public StoryLineObject[] GoodPointPromptList;

	//UI
	public RectTransform BackImage;
	public RectTransform EnterButton;
	public Animator BackImageAnim;
	public GameObject BackImageObj;

	//Input
	public JoyInput[] JoyInput;

	public void Init()
	{
		StoryLineObjectData = Object.FindObjectsOfType(typeof(StoryLineObject)) as StoryLineObject[];
		BackImageObj.SetActive(false);
	}

	public void PlayStoryLine(string _ID,bool PlayNow = false)
	{
		if (PlayNow)
		{
			BackImageObj.SetActive(true);

			foreach (StoryLineObject obj in StoryLineObjectData)
			{
				if (obj.StoryLineID == _ID)
				{
					Debug.Log(obj.CH);
					mTimer = 0;
					CurLine = obj;
					TextUI.text = CurLine.CH;
					SetBackGroundSize();
					BackImageAnim.SetTrigger("Show");
					return;
				}
			}
		}
		else
		{
			BackImageObj.SetActive(true);

			foreach (StoryLineObject obj in StoryLineObjectData)
			{
				if (obj.StoryLineID == _ID)
				{
					Debug.Log(obj.CH);
					mTimer = 0;
					CurLine = obj;
					TextUI.text = CurLine.CH;
					SetBackGroundSize();

					return;
				}
			}
		}
	}

	public void StopStoryLine(StoryLineObject _StoryLineObject)
	{
		if (_StoryLineObject.NextLineID == "")
		{
			CurLine = null;
		}
		else
		{
			PlayStoryLine(_StoryLineObject.NextLineID);
			mDoDelayStartLine = true;
			mDelayTimer = mNextLineStartDelay;
		}
		BackImageAnim.SetTrigger("Hide");
	}




	private void Start()
    {
		Init();
		//PlayStoryLine("StartLine_02");
	}


	private void Update()
    {
		if(CurLine != null)
		{
			LineTimer();
		}


		if (mDoDelayStartLine)
		{
			mDelayTimer += Time.deltaTime;
			if (mDelayTimer >= mNextLineStartDelay)
			{
				PlayStoryLine(mNextLine);
				mDoDelayStartLine = false;
				BackImageAnim.SetTrigger("Show");
			}
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			PlayStoryLine("GameStartLine_04");
		}

	}




	private void LineTimer()
	{
		mTimer += Time.deltaTime;
		if (mTimer >= CurLine.Duration)
		{
			if (!CurLine.AutoFinish)
			{
				EnterButton.gameObject.SetActive(true);

				for (int i = 0; i < JoyInput.Length; i++)
				{
					if (JoyInput[i].GetJoyAttack())
					{
						StopStoryLine(CurLine);
						return;
					}
				}
			}
			else
			{
				StopStoryLine(CurLine);
			}
		}
	}




	private void SetBackGroundSize()
	{
		float BackGroundWidth;

		switch (CurLanguage)
		{
			case Language.CH:
				BackGroundWidth = CurLine.CH.Length * 25f + 250f;
				BackImage.sizeDelta = new Vector2(BackGroundWidth, BackImage.sizeDelta.y);

				if (!CurLine.AutoFinish)
				{
					EnterButton.gameObject.SetActive(false);
					EnterButton.anchoredPosition = new Vector2(BackGroundWidth / 2 - 120f, EnterButton.anchoredPosition.y);
				}
				else
				{
					EnterButton.gameObject.SetActive(false);
				}
				break;

			case Language.EN:
				break;

			case Language.JP:
				break;
		}
	}

	private float  mTimer;

	private bool mDoDelayStartLine;
	private string mNextLine;
	private float  mNextLineStartDelay = 0.5f;
	private float  mDelayTimer;


}
