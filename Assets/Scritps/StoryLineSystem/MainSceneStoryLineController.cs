﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine.UI;

public enum MainScenePhase
{
	Phase1_GO,
	Phase2_WaitMove,
	Phase3_WaitAttack,
}



public class MainSceneStoryLineController : MonoBehaviour
{
	public MainScenePhase State = MainScenePhase.Phase1_GO;
	public StoryLineManager StoryLineManager;
	public CameraManager CameraManager;

	public CharacterAnimation PlayerA;
	public CharacterAnimation PlayerB;
	public CharacterAnimation PlayerC;
	public CharacterAnimation PlayerD;


	public RawImage FadOutImg;

	public float StartTime = 3f;




	void Start()
	{

	}



	void Update()
	{
		timer += Time.deltaTime;
		if (timer >= StartTime && !isStart)
		{
			StoryLineManager.PlayStoryLine("GameStartLine_04");
			isStart = true;
		}


		if (StoryLineManager.CurLine == null)
		{
			return;
		}
		switch (StoryLineManager.CurLine.StoryLineID)
		{
			case "GameStartLine_06":
				if (State != MainScenePhase.Phase2_WaitMove)
				{
					State = MainScenePhase.Phase2_WaitMove;
				}
				//if(PlayerA.mIsWalk)
				//{
				//	PlayerAMove = true;
				//}
				//if (PlayerB.mIsWalk)
				//{
				//	PlayerBMove = true;
				//}
				//if (PlayerC.mIsWalk)
				//{
				//	PlayerCMove = true;
				//}
				//if (PlayerD.mIsWalk)
				//{
				//	PlayerDMove = true;
				//}
				//if(PlayerDMove && PlayerAMove && PlayerBMove && PlayerCMove)
				//{
				//	StoryLineManager.StopStoryLine (StoryLineManager.CurLine);
				//}

				break;

			case "GameStartLine_07":
				if (State != MainScenePhase.Phase3_WaitAttack)
				{
					State = MainScenePhase.Phase3_WaitAttack;
				}
				//if (PlayerA.IsPlayingAttackAnim())
				//{
				//	PlayerAAttack = true;
				//}
				//if (PlayerB.IsPlayingAttackAnim())
				//{
				//	PlayerBAttack = true;
				//}
				//if (PlayerC.IsPlayingAttackAnim())
				//{
				//	PlayerCAttack = true;
				//}
				//if (PlayerD.IsPlayingAttackAnim())
				//{
				//	PlayerDAttack = true;
				//}
				//if (PlayerAAttack && PlayerBAttack && PlayerCAttack && PlayerCAttack)
				//{
				//	StoryLineManager.StopStoryLine(StoryLineManager.CurLine);
				//}
				break;
			case "GameStartLine_10":

				GameManager.s_Instance.GoSpawn();
				break;
		}
	}

	private bool PlayerAMove = false;
	private bool PlayerBMove = false;
	private bool PlayerCMove = false;
	private bool PlayerDMove = false;

	private bool PlayerAAttack = false;
	private bool PlayerBAttack = false;
	private bool PlayerCAttack = false;
	private bool PlayerDAttack = false;


	private float timer;
	private float fadoutTimer;
	private bool isStart = false;
}
