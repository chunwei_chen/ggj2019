﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum BadEndPhase
{
	Phase0,
	Phase1_Over,
}



public class BadEndStoryLineController : MonoBehaviour
{
	public BadEndPhase State = BadEndPhase.Phase0;
	public StoryLineManager StoryLineManager;
	public CameraManager CameraManager;

	//public GameObject AnimationObj;

	//public GameObject MakerObj;

	//public RawImage FadOutImg;

	public float StartTime = 1f;




	void Start()
	{

	}



	void Update()
	{
		timer += Time.deltaTime;
		if (timer >= StartTime && !isStart)
		{
			StoryLineManager.PlayStoryLine("BadEnd_01");
			isStart = true;
		}

		if (State == BadEndPhase.Phase1_Over)
		{
			LoadTimer -= Time.deltaTime;

			if (LoadTimer <= 0f)
			{
				FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
				masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
				SceneManager.LoadScene(2);
			}
		}


		if (StoryLineManager.CurLine == null)
		{
			return;
		}


		switch (StoryLineManager.CurLine.StoryLineID)
		{
			case "BadEnd_03":
				if (State != BadEndPhase.Phase1_Over)
				{
					State = BadEndPhase.Phase1_Over;
				}
				break;
		}

	}





	private float LoadTimer = 5f;


	private float timer;
	private float fadoutTimer;
	private bool isStart = false;
}
