﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine.UI;

using UnityEngine.SceneManagement;
public enum GoodEndPhase
{
	Phase0,
	Phase1_WaitAnimation,
	Phase2_ShowPeople,
}



public class GoodEndStoryLineController : MonoBehaviour
{
	public GoodEndPhase State = GoodEndPhase.Phase0;
	public StoryLineManager StoryLineManager;
	public CameraManager CameraManager;

	public GameObject AnimationObj;

	public GameObject MakerObj;

	public RawImage FadOutImg;

	public float StartTime = 1f;




	void Start()
	{

	}



	void Update()
	{
		timer += Time.deltaTime;
		if (timer >= StartTime && !isStart)
		{
			StoryLineManager.PlayStoryLine("GoodEnd_01");
			isStart = true;
		}

		if (isPlayAnim)
		{
			AnimationTimer -= Time.deltaTime;

			if (AnimationTimer <= 0f)
			{
				StoryLineManager.PlayStoryLine("GoodEnd_04",true);
				isPlayAnim = false;
			}
		}

		if(State == GoodEndPhase.Phase2_ShowPeople)
		{
			PeopleTimer -= Time.deltaTime;

			if (PeopleTimer <= 0f)
			{
				Debug.Log("Show People!!!!!!!!!!!!");

				EndTimer -= Time.deltaTime;

				if (EndTimer <= 0f)
				{
					FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
					masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
					SceneManager.LoadScene(0);

				}
			}
		}

		if (StoryLineManager.CurLine == null)
		{
			Debug.Log("AAAA");
			return;
		}



		switch (StoryLineManager.CurLine.StoryLineID)
		{
			case "GoodEnd_03":
				if (State != GoodEndPhase.Phase1_WaitAnimation)
				{
					State = GoodEndPhase.Phase1_WaitAnimation;
				}

				AnimationTimer -= Time.deltaTime;

				if (AnimationTimer <= 8f && !isPlayAnim)
				{
					AnimationObj.SetActive(true);
					isPlayAnim = true;
				}
				break;

			case "GoodEnd_04":
				if (State != GoodEndPhase.Phase2_ShowPeople)
				{
					State = GoodEndPhase.Phase2_ShowPeople;
				}

				PeopleTimer -= Time.deltaTime;

				if (PeopleTimer <= 0f)
				{
					Debug.Log("Show People!!!!!!!!!!!!");
					StoryLineManager.StopStoryLine(StoryLineManager.CurLine);
					MakerObj.SetActive(true);
					EndTimer -= Time.deltaTime;

					if (EndTimer <= 0f)
					{
						Debug.Log("Load !!!!!!!!!");

					}
				}

				break;
		}

	}


	private float AnimationTimer = 10f;
	private bool isPlayAnim = false;
	private float PeopleTimer = 5f;
	private float EndTimer = 5f;





	private float timer;
	private float fadoutTimer;
	private bool isStart = false;
}
