﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum StartScenePhase
{
	Phase1_Hello,
	Phase2_LeaveOrStay,
	Phase3_AddDad,
	Phase4_AddMom,
	Phase5_AddGrand,
	Phase6_AddSisbro,
	Phase7_ShowHouse,

	Phase8_Earthquake,
	Phase9_WindowBreak,
}


public class StartSceneManager : MonoBehaviour
{

	public StartScenePhase State = StartScenePhase.Phase1_Hello;
	public StoryLineManager StoryLineManager;
	public CameraManager    CameraManager;

	public GameObject LeaveOrStayObj;
	public GameObject Dad;
	public GameObject Mam;
	public GameObject Grand;
	public GameObject Sisbor;
	public GameObject BGGroup;
	public GameObject Light1;
	public GameObject Light2;
	public RawImage FadOutImg;

	public float StartTime = 3f;



	//public void 




	void Start()
    {

	}



	void Update()
    {
		if (Input.GetKeyDown(KeyCode.G))
		{
			FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
			masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
			SceneManager.LoadScene(2);
		}

		timer += Time.deltaTime;
		if (timer >= StartTime && !isStart)
		{
			StoryLineManager.PlayStoryLine("StartLine_01");
			isStart = true;
		}


		if (StoryLineManager.CurLine == null)
		{
			return;
		}
		switch (StoryLineManager.CurLine.StoryLineID)
		{
			case "StartLine_02":
				if (State != StartScenePhase.Phase2_LeaveOrStay)
				{
					State = StartScenePhase.Phase2_LeaveOrStay;
					//LeaveOrStayObj.SetActive(true);
				}
				break;

			case "StartLine_08":
				if (State != StartScenePhase.Phase3_AddDad)
				{
					State = StartScenePhase.Phase3_AddDad;
					CameraManager.SetPlayerSolo(0);
					Dad.SetActive(true);
				}
				break;

			case "StartLine_09":
				if (State != StartScenePhase.Phase4_AddMom)
				{
					State = StartScenePhase.Phase4_AddMom;
					CameraManager.SetPlayerSolo(1);
					Mam.SetActive(true);
				}
				break;

			case "StartLine_10":
				if (State != StartScenePhase.Phase5_AddGrand)
				{
					State = StartScenePhase.Phase5_AddGrand;
					CameraManager.SetPlayerSolo(2);
					Grand.SetActive(true);
				}
				break;

			case "StartLine_11":
				if (State != StartScenePhase.Phase6_AddSisbro)
				{
					State = StartScenePhase.Phase6_AddSisbro;
					CameraManager.SetPlayerSolo(3);
					Sisbor.SetActive(true);
				}
				break;

			case "StartLine_14":
				if (State != StartScenePhase.Phase7_ShowHouse)
				{
					State = StartScenePhase.Phase7_ShowHouse;
					CameraManager.EndPlayerSolo();
					BGGroup.SetActive(true);
				}
				break;

			case "GameStartLine_01":
				if (State != StartScenePhase.Phase8_Earthquake)
				{
					State = StartScenePhase.Phase8_Earthquake;
					ProCamera2DShake.Instance.Shake("LargeExplosion");
				}
				break;

			case "GameStartLine_03":
				if (State != StartScenePhase.Phase9_WindowBreak)
				{
					State = StartScenePhase.Phase9_WindowBreak;
					ProCamera2DShake.Instance.Shake("LargeExplosion");
					Light1.SetActive(false);
					Light2.SetActive(true);

				}
				fadoutTimer += Time.deltaTime;
				if (fadoutTimer > 2f)
				{
					FadOutImg.color = new Color(FadOutImg.color.r, FadOutImg.color.g, FadOutImg.color.b, fadoutTimer - 2f);
				}
				if (fadoutTimer > 3f)
				{
					FMOD.Studio.Bus masterBus = FMODUnity.RuntimeManager.GetBus("bus:/");
					masterBus.stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
					SceneManager.LoadScene(2);
				}
					break;
		}
    }


	private float timer;
	private float fadoutTimer;
	private bool isStart = false;
}
