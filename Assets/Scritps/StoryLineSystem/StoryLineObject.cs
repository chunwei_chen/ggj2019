﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StoryLineObject : MonoBehaviour
{ 
	public string StoryLineID;

	public StoryLineType StoryLineType;

	public float Duration;

	public string ENG;
	public string JP;
	public string CH;

	public string NextLineID;
	public bool   AutoFinish;
}
