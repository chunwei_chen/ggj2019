﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
	public Animator mAnim;
	public bool mIsFacingRight = false;
	public bool mIsWalk = false;
	public int mIsCombo = 0;
	public string[] mAttackAnimName;

	void Start()
	{
		mAnim = gameObject.GetComponent<Animator>();
	}
    void Update()
	{
		mAnim.SetBool("IsWalk", mIsWalk);
		mAnim.SetInteger("IsCombo", mIsCombo);
	}
	public bool SetFacing(float aInput)
	{
		if (aInput > 0)
		{
			if (!mIsFacingRight)
			{
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				mIsFacingRight = true;
			}
			return true;
		}
		else
		{
			if (mIsFacingRight)
			{
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				mIsFacingRight = false;
			}
			return false;
		}
	}
	public void AttackAnim()
	{
		mAnim.SetTrigger("IsAttack");
	}
	public void HurtAnim()
	{
		mAnim.SetTrigger("IsHurt");
	}
}
